import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { PruebaArrayComponent } from './components/prueba-array/prueba-array.component';
import { PruebaDictComponent } from './components/prueba-dict/prueba-dict.component';
import { PruebaService } from './components/services/prueba.service';

@NgModule({
  declarations: [
    AppComponent,
    PruebaArrayComponent,
    PruebaDictComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    PruebaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
