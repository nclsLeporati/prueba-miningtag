import { Injectable } from '@angular/core';
import { IRemoteResponse } from 'src/app/models/remote.response';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PruebaService {
  private readonly url = 'http://168.232.165.184/prueba';

  constructor(private readonly http: HttpClient) { }

  public getArray(): Observable<IRemoteResponse> {
    return this.http.get<IRemoteResponse>(`${this.url}/array`);
  }

  public getDict(): Observable<IRemoteResponse> {
    return this.http.get<IRemoteResponse>(`${this.url}/dict`);
  }
}
