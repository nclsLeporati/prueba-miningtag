import { Component, OnInit } from '@angular/core';
import { PruebaService } from '../services/prueba.service';
import { IDictionaryModel } from 'src/app/models/dictionary.model';

@Component({
  selector: 'app-prueba-dict',
  templateUrl: './prueba-dict.component.html'
})
export class PruebaDictComponent implements OnInit {
  public loading: boolean;
  public data: Array<IDictionaryModel> = new Array();
  public error: string;

  public readonly dict = [
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
    'n', 'o', 'p', 'q', 'r', 's', 'u', 'v', 'w', 'x', 'y', 'z'
  ];

  public readonly word = 'paragraph';

  constructor(
    private readonly pruebaService: PruebaService
  ) { }

  ngOnInit() {
    // this.getDict();
  }

  private getDict(): void {
    this.loading = true;
    this.pruebaService.getDict().subscribe(
      (res) => {
        console.log(res);
        this.data = (res.success) ? res.data : [];
        this.error = (!res.success) ? res.error || 'Error de servicio' : null;
      },
      () => { this.error = 'Error de servicio'; },
      () => { this.loading = false; }
    );
  }

  public getLetterValue(letter: string, index: number): number {
    let total = 0;
    const letters = this.data[index].paragraph.toLowerCase().split('');
    letters.filter(lett => lett === letter).forEach(lett => {
      if (this.word.includes(lett)) {
        total += 1;
      }
    });
    return total;
  }

  public getNumberTotal(item: IDictionaryModel): number {
    let total = 0;
    item.paragraph.split('').forEach(
      val => {
        if (Number(val)) {
          total += Number(val);
        }
    });
    return total;
  }
}
