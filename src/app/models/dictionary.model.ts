export interface IDictionaryModel {
  hasCopyright: boolean;
  number: 30;
  paragraph: string;
}
