import { Component, OnInit } from '@angular/core';
import { PruebaService } from '../services/prueba.service';

@Component({
  selector: 'app-prueba-array',
  templateUrl: './prueba-array.component.html'
})
export class PruebaArrayComponent implements OnInit {
  public loading: boolean;
  public data: Array<any> = new Array();
  public error: string;

  constructor(
    private readonly pruebaService: PruebaService
  ) { }

  ngOnInit() {
    // this.getArray();
  }

  public getArray(): void {
    if (this.loading) {
      return;
    }

    this.loading = true;
    this.pruebaService.getArray().subscribe(
      (res) => {
        console.log(res);
        this.data = (res.success) ? res.data : [];
        this.error = (!res.success) ? res.error || 'Error de servicio' : null;
      },
      () => { this.error = 'Error de servicio'; },
      () => { this.loading = false; }
    );
  }

  public get numbersList(): Set<number> {
    return new Set<number>(this.data);
  }

  public getQuantity(item: number): number {
    return this.data.filter(num => num === item).length;
  }

  public getFirstPosition(item: number): number {
    return this.data.indexOf(item);
  }

  public getLastPosition(item: number): number {
    return this.data.lastIndexOf(item);
  }

  public getQuantityColor(item: number): string {
    const quantity = this.getQuantity(item) - 1;
    let style = 'transparent';
    if (quantity >= 2) {
      style = 'green';
    } else if (quantity === 1) {
      style = 'yellow';
    } else if (quantity < 1) {
      style = 'lightgrey';
    }
    return style;
  }

  public getSortedNumbers(): string {
      const list = Array.from(this.numbersList);
      const len = list.length;
      for (let i = 0; i < len; i++) {
        for (let j = 0; j < len; j++) {
          if (list[j] > list[j + 1]) {
            const tmp = list[j];
            list[j] = list[j + 1];
            list[j + 1] = tmp;
          }
        }
      }
      return list.join(', ');
  }

}
